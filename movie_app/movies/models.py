from django.db import models
from django_prometheus.models import ExportModelOperationsMixin

from movie_app.models import BaseModel


class Comment(ExportModelOperationsMixin('comment'), BaseModel):
    movie = models.ForeignKey(
        'movies.Movie',
        on_delete=models.PROTECT,
    )
    body = models.TextField()


class Movie(ExportModelOperationsMixin('movie'), BaseModel):
    title = models.CharField(
        max_length=128,
    )
    year = models.PositiveSmallIntegerField(
        null=True,
    )
    released = models.DateField(
        null=True,
    )
    runtime = models.PositiveSmallIntegerField(
        null=True,
    )
    genres = models.ManyToManyField(
        'movies.Genre',
    )
    directors = models.ManyToManyField(
        'movies.Person',
        related_name='movies_directed_by',
    )
    writers = models.ManyToManyField(
        'movies.Person',
        related_name='movies_written_by',
    )
    actors = models.ManyToManyField(
        'movies.Person',
        related_name='movies_acted_by',
    )
    plot = models.TextField(
        null=True,
    )
    languages = models.ManyToManyField(
        'movies.Language',
        related_name='movies',
    )
    countries = models.ManyToManyField(
        'movies.Country',
        related_name='movies',
    )
    awards = models.TextField(
        null=True,
    )
    poster = models.URLField(
        null=True,
    )
    metascore = models.PositiveSmallIntegerField(
        null=True,
    )
    dvd = models.DateField(
        null=True,
    )
    box_office = models.PositiveIntegerField(
        null=True,
    )
    producer = models.ForeignKey(
        'movies.Producer',
        related_name='movies',
        on_delete=models.PROTECT,
        null=True,
    )
    website = models.URLField(
        null=True,
    )
    imdb_votes = models.PositiveIntegerField(
        null=True,
    )
    imdb_rating = models.DecimalField(
        decimal_places=1,
        max_digits=3,
        null=True,
    )


class Producer(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
    )

    def __str__(self):
        return self.name


class RatingSource(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
    )

    def __str__(self):
        return self.name


class Rating(models.Model):
    movie = models.ForeignKey(
        'movies.Movie',
        related_name='ratings',
        on_delete=models.PROTECT,
    )
    source = models.ForeignKey(
        'movies.RatingSource',
        related_name='ratings',
        on_delete=models.PROTECT,
    )
    value = models.CharField(
        max_length=16,
    )


class Country(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'countries'


class Language(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
    )

    def __str__(self):
        return self.name


class Person(models.Model):
    name = models.CharField(
        max_length=128,
        unique=True,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'people'


class Genre(models.Model):
    name = models.CharField(
        max_length=64,
        unique=True,
    )

    def __str__(self):
        return self.name

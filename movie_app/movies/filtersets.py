from django_filters import rest_framework as filters

from movies import models


class MovieFilterSet(filters.FilterSet):
    min_year = filters.NumberFilter(
        field_name='year',
        lookup_expr='gte',
    )
    max_year = filters.NumberFilter(
        field_name='year',
        lookup_expr='lte',
    )
    genres = filters.ModelMultipleChoiceFilter(
        field_name='genres__name',
        lookup_expr='in',
        queryset=models.Genre.objects.all(),
        to_field_name='name',
    )

    class Meta:
        model = models.Movie
        fields = (
            'year',
            'min_year',
            'max_year',
            'genres',
        )


class CreatedAtDateCommentFilterSet(filters.FilterSet):
    start_date = filters.DateFilter(
        field_name='created_at',
        lookup_expr='date__gte',
        required=True,
    )
    end_date = filters.DateFilter(
        field_name='created_at',
        lookup_expr='date__lte',
        required=True,
    )

    class Meta:
        model = models.Comment
        fields = (
            'start_date',
            'end_date',
        )

import logging
from datetime import datetime
from decimal import Decimal

import requests
from django.conf import settings

from movies import exceptions

IMDB_URL = 'http://www.omdbapi.com/'
DATE_FORMAT = '%d %b %Y'

logger = logging.getLogger('imdb_client')


def split_and_remove_comments(data):
    return [d.split(' (')[0] for d in data.split(', ')]


class ImdbParser:
    def __init__(self, data):
        self.data = data
        self.result = {}

    def parse(self):
        for name, mapping, result_name in (
                (
                        'imdbRating',
                        Decimal,
                        'imdb_rating',
                ),
                (
                        'imdbVotes',
                        lambda x: int(x.replace(',', '')),
                        'imdb_votes',
                ),
                (
                        'BoxOffice',
                        lambda x: int(x.replace(',', '')[1:]),
                        'box_office',
                ),
                (
                        'Genre',
                        lambda x: x.split(', '),
                        'genres',
                ),
                (
                        'Director',
                        split_and_remove_comments,
                        'directors',
                ),
                (
                        'Writer',
                        split_and_remove_comments,
                        'writers',
                ),
                (
                        'Language',
                        split_and_remove_comments,
                        'languages',
                ),
                (
                        'Country',
                        split_and_remove_comments,
                        'countries',
                ),
                (
                        'Production',
                        lambda x: x,
                        'producer',
                )
        ):
            self.set_field(name, mapping, result_name)
        for name, mapping in (
                (
                        'DVD',
                        lambda x: datetime.strptime(x, DATE_FORMAT).date(),
                ),
                (
                        'Released',
                        lambda x: datetime.strptime(x, DATE_FORMAT).date(),
                ),
                (
                        'Year',
                        int,
                ),
                (
                        'Year',
                        int,
                ),
                (
                        'Runtime',
                        lambda x: int(x.split()[0]),
                ),
                (
                        'Actors',
                        split_and_remove_comments,
                ),
                (
                        'Plot',
                        lambda x: x,
                ),
                (
                        'Awards',
                        lambda x: x,
                ),
                (
                        'Poster',
                        lambda x: x,
                ),
                (
                        'Website',
                        lambda x: x,
                ),
                (
                        'Metascore',
                        int,
                ),

                (
                        'Title',
                        lambda x: x,
                ),
        ):
            self.set_field(name, mapping)
        self.set_ratings()
        return self.result

    def set_ratings(self):
        ratings = self.data.get('Ratings')
        if ratings and ratings != 'N/A':
            try:
                self.result['ratings'] = [
                    {
                        'source': rating['Source'],
                        'value': rating['Value'],
                    } for rating in ratings
                ]
            except:  # noqa
                logger.error(
                    f'Imdb data ratings parsing failed: {self.data}'
                )
                raise exceptions.ImdbDataParsingException()

    def set_field(self, source_name, func, result_name=None):
        result_name = result_name or source_name.lower()
        value = self.data.get(source_name)
        if value and value != 'N/A':
            try:
                self.result[result_name] = func(value)
            except:  # noqa
                logger.error(
                    f'Imdb data {source_name} parsing failed: {self.data}'
                )
                raise exceptions.ImdbDataParsingException()


def get_movie_data(title):
    response = requests.get(
        IMDB_URL,
        params={
            'apikey': settings.IMDB_APIKEY,
            't': title,
            'type': 'movie',
            'plot': 'full',
        },
    )
    if response.status_code != 200:
        raise exceptions.ImdbCallFailedException()
    data = response.json()
    if data['Response'] != 'True':
        raise exceptions.MovieNotFoundException()
    return ImdbParser(data).parse()

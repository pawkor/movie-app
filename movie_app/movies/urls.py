from django.urls import path, include
from rest_framework import routers

from movies import viewsets


router = routers.SimpleRouter()
router.register(
    r'movies',
    viewsets.MovieViewSet,
    basename='movies',
)
router.register(
    r'genres',
    viewsets.GenreViewset,
    basename='genres',
)
router.register(
    r'comments',
    viewsets.CommentViewSet,
    basename='comments',
)
router.register(
    r'top',
    viewsets.TopMovieViewSet,
    basename='top-movies',
)

urlpatterns = [
    path(
        'v1/',
        include(
            (
                (
                    path('', include(router.urls)),
                ),
                'movies',
            ),
            namespace='v1_movies',
        ),
    )
]

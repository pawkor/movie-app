from movies import models


def get_or_create_models_by_name(model, names):
    objects = []
    for name in names:
        obj, _ = model.objects.get_or_create(
            name=name,
        )
        objects.append(obj)
    return objects


def create_rating(source, value, movie):
    rating_source, _ = models.RatingSource.objects.get_or_create(
        name=source,
    )
    models.Rating.objects.create(
        movie=movie,
        source=rating_source,
        value=value,
    )

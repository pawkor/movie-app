class MovieNotFoundException(Exception):
    pass


class ImdbCallFailedException(Exception):
    pass


class ImdbDataParsingException(Exception):
    pass

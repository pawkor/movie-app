from rest_framework import serializers

from movies import models


class RatingSerializer(serializers.ModelSerializer):
    source = serializers.StringRelatedField()

    class Meta:
        model = models.Rating
        fields = (
            'source',
            'value',
        )


class MovieSerializer(serializers.ModelSerializer):
    genres = serializers.StringRelatedField(
        many=True,
        read_only=True,
    )
    directors = serializers.StringRelatedField(
        many=True,
        read_only=True,
    )
    writers = serializers.StringRelatedField(
        many=True,
        read_only=True,
    )
    actors = serializers.StringRelatedField(
        many=True,
        read_only=True,
    )
    languages = serializers.StringRelatedField(
        many=True,
        read_only=True,
    )
    countries = serializers.StringRelatedField(
        many=True,
        read_only=True,
    )
    producer = serializers.StringRelatedField()
    ratings = RatingSerializer(
        many=True,
        read_only=True,
    )

    class Meta:
        model = models.Movie
        fields = (
            'id',
            'title',
            'year',
            'released',
            'runtime',
            'genres',
            'directors',
            'writers',
            'actors',
            'plot',
            'languages',
            'countries',
            'awards',
            'poster',
            'metascore',
            'dvd',
            'box_office',
            'producer',
            'website',
            'ratings',
            'imdb_rating',
            'imdb_votes',
        )
        read_only_fields = (
            'year',
            'released',
            'runtime',
            'plot',
            'awards',
            'poster',
            'metascore',
            'dvd',
            'box_office',
            'website',
            'imdb_rating',
            'imdb_votes',
        )


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Genre
        fields = (
            'name',
        )


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Comment
        fields = (
            'id',
            'movie',
            'body',
        )


class TopMovieSerializer(serializers.Serializer):
    movie_id = serializers.IntegerField()
    total_comments = serializers.IntegerField()
    rank = serializers.IntegerField()

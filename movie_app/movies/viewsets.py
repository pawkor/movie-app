from django.db.models import Count, Window, F
from django.db.models.functions import DenseRank
from django.db.transaction import atomic
from rest_framework import viewsets, mixins, filters
from rest_framework import exceptions as rest_exceptions
from django_filters.rest_framework import DjangoFilterBackend

from movies import (
    models,
    serializers,
    filtersets,
    imdb_client,
    service,
    exceptions,
)


class MovieViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    queryset = models.Movie.objects.select_related(
        'producer',
    ).prefetch_related(
        'genres',
        'directors',
        'writers',
        'actors',
        'languages',
        'countries',
        'ratings',
        'ratings__source',
    ).all()
    serializer_class = serializers.MovieSerializer
    filter_backends = (
        DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    search_fields = ('title',)
    ordering_fields = ('year', 'imdb_votes', 'imdb_rating')
    filterset_class = filtersets.MovieFilterSet

    def filter_queryset(self, queryset):
        try:
            return super().filter_queryset(queryset)
        except rest_exceptions.ValidationError:
            return queryset.none()

    def perform_create(self, serializer):
        movie_data = self._get_movie_data(serializer)
        self._create_movie(movie_data, serializer)

    def _create_movie(self, movie_data, serializer):
        with atomic():
            for field, model in (
                    ('genres', models.Genre),
                    ('directors', models.Person),
                    ('writers', models.Person),
                    ('actors', models.Person),
                    ('languages', models.Language),
                    ('countries', models.Country),
            ):
                movie_data[field] = service.get_or_create_models_by_name(
                    model,
                    movie_data.pop(field, [])
                )
            producer = movie_data.pop('producer', None)
            if producer:
                movie_data['producer'] = service.get_or_create_models_by_name(
                    models.Producer,
                    [producer]
                )[0]
            ratings = movie_data.pop('ratings', [])
            movie = serializer.save(
                **movie_data,
            )
            for rating in ratings:
                service.create_rating(
                    rating['source'],
                    rating['value'],
                    movie,
                )

    def _get_movie_data(self, serializer):
        try:
            movie_data = imdb_client.get_movie_data(
                serializer.validated_data['title'],
            )
        except exceptions.MovieNotFoundException:
            raise rest_exceptions.ValidationError(
                'Movie not found',
            )
        except exceptions.ImdbCallFailedException:
            raise rest_exceptions.ValidationError(
                'Imbd not working, please try later',
            )
        except exceptions.ImdbDataParsingException:
            raise rest_exceptions.ValidationError(
                'Imdb data not parsed successfully'
            )
        return movie_data


class GenreViewset(
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    queryset = models.Genre.objects.all()
    serializer_class = serializers.GenreSerializer


class CommentViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    queryset = models.Comment.objects.all()
    serializer_class = serializers.CommentSerializer
    filter_backends = (
        DjangoFilterBackend,
    )
    filter_fields = ('movie',)


class TopMovieViewSet(
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    queryset = models.Comment.objects.all()
    serializer_class = serializers.TopMovieSerializer
    filter_backends = (
        DjangoFilterBackend,
    )
    filterset_class = filtersets.CreatedAtDateCommentFilterSet

    def filter_queryset(self, queryset):
        dense_rank_by_total_comments = Window(
            expression=DenseRank(),
            order_by=F("total_comments").desc(),
        )

        return super().filter_queryset(queryset).values(
            'movie_id',
        ).annotate(
            total_comments=Count('id'),
        ).annotate(
            rank=dense_rank_by_total_comments,
        ).order_by(
            '-total_comments',
            'movie_id',
        )

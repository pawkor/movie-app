import pytest
from django.urls import reverse

from movies.tests import factories


@pytest.mark.django_db
def test__list_genres(client):
    factories.GenreFactory(
        name='Test',
    )

    url = reverse('api:v1_movies:genres-list')

    response = client.get(url)

    assert response.status_code == 200
    assert response.json() == {
        'count': 1,
        'next': None,
        'previous': None,
        'results': [
            {
                'name': 'Test',
            },
        ],
    }

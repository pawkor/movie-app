from datetime import datetime

import pytest
from django.urls import reverse

from movies.tests import factories


@pytest.mark.parametrize(
    'data,expected_status_code',
    (
            ({}, 400),
            ({'start_date': '2012-10-10'}, 400),
            ({'end_date': '2012-10-10'}, 400),
            ({'start_date': '2012-10-10', 'end_date': '2014-10-10'}, 200),
    )
)
@pytest.mark.django_db
def test__list_top_movies__requires_start_date_and_end_date(
        data, expected_status_code, client
):
    url = reverse('api:v1_movies:top-movies-list')

    response = client.get(
        url,
        data=data,
    )

    assert response.status_code == expected_status_code


def create_comment(movie, created_at, size=1):
    comments = factories.CommentFactory.create_batch(
        size,
        movie=movie,
    )
    for comment in comments:
        comment.created_at = created_at
        comment.save()


@pytest.mark.django_db
def test__list_top_movies_filter_by_start_and_end_date(client):
    movie_1 = factories.MovieFactory()
    create_comment(
        movie=movie_1,
        created_at=datetime(year=2015, month=10, day=10),
        size=10,
    )
    create_comment(
        movie=movie_1,
        created_at=datetime(year=2014, month=10, day=10),
    )
    create_comment(
        movie=movie_1,
        created_at=datetime(year=2001, month=10, day=10),
    )
    movie_2 = factories.MovieFactory()
    create_comment(
        movie=movie_2,
        created_at=datetime(year=2000, month=10, day=10),
    )
    movie_3 = factories.MovieFactory()
    create_comment(
        movie=movie_3,
        created_at=datetime(year=2012, month=10, day=10),
        size=20
    )
    movie_4 = factories.MovieFactory()
    create_comment(
        movie=movie_4,
        created_at=datetime(year=2012, month=10, day=10),
        size=20
    )
    movie_5 = factories.MovieFactory()
    create_comment(
        movie=movie_5,
        created_at=datetime(year=2010, month=10, day=10, hour=0, minute=0, second=0),  # noqa
    )
    create_comment(
        movie=movie_5,
        created_at=datetime(year=2020, month=10, day=10, hour=23, minute=59, second=59),  # noqa
    )
    url = reverse('api:v1_movies:top-movies-list')

    response = client.get(
        url,
        data={
            'start_date': '2010-10-10',
            'end_date': '2020-10-10',
        },
    )

    assert response.status_code == 200
    assert response.json() == {
        'count': 4,
        'next': None,
        'previous': None,
        'results': [
            {
                'movie_id': movie_3.id,
                'total_comments': 20,
                'rank': 1,
            },
            {
                'movie_id': movie_4.id,
                'total_comments': 20,
                'rank': 1,
            },
            {
                'movie_id': movie_1.id,
                'total_comments': 11,
                'rank': 2,
            },
            {
                'movie_id': movie_5.id,
                'total_comments': 2,
                'rank': 3,
            },
        ],
    }

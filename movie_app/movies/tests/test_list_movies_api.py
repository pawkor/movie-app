import pytest
from django.urls import reverse

from movies.tests import factories


@pytest.mark.django_db
def test__list_movies(client):
    movie = factories.MovieFactory(
        title='Shrek',
        year='2001',
        released='2001-05-18',
        runtime=90,
        genres=[
            factories.GenreFactory(
                name='Animation',
            ),
            factories.GenreFactory(
                name='Adventure',
            ),
            factories.GenreFactory(
                name='Comedy',
            ),
        ],
        directors=[
            factories.PersonFactory(
                name='Andrew Adamson',
            ),
            factories.PersonFactory(
                name='Vicky Jenson',
            ),
        ],
        writers=[
            factories.PersonFactory(
                name='William Steig',
            ),
            factories.PersonFactory(
                name='Ted Elliott',
            )
        ],
        actors=[
            factories.PersonFactory(
                name='Mike Myers',
            ),
            factories.PersonFactory(
                name='Eddie Murphy',
            ),
        ],
        plot='''When a green ogre named Shrek discovers his swamp (...)''',
        languages=[
            factories.LanguageFactory(
                name='English',
            )
        ],
        countries=[
            factories.CountryFactory(
                name='USA',
            )
        ],
        awards='Won 1 Oscar. Another 36 wins & 60 nominations',
        poster='https://m.media-amazon.com/images/M/MV5BOGZhM2FhNTItODAzNi00YjA0LWEyN2UtNjJlYWQzYzU1MDg5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',  # noqa
        metascore=84,
        dvd='2001-11-02',
        box_office=266982666,
        producer__name='Dreamworks',
        website='http://www.shrek.com/',
        imdb_votes=123,
        imdb_rating=2.3,
    )
    factories.RatingFactory(
        movie=movie,
        source__name='Internet Movie Database',
        value='7.9/10',
    )
    factories.RatingFactory(
        movie=movie,
        source__name='Rotten Tomatoes',
        value='88%',
    )

    url = reverse('api:v1_movies:movies-list')

    response = client.get(url)

    assert response.status_code == 200
    assert response.json() == {
        'count': 1,
        'next': None,
        'previous': None,
        'results': [
            {
                'actors': ['Mike Myers', 'Eddie Murphy'],
                'awards': 'Won 1 Oscar. Another 36 wins & 60 nominations',
                'box_office': 266982666,
                'countries': ['USA'],
                'directors': ['Andrew Adamson', 'Vicky Jenson'],
                'dvd': '2001-11-02',
                'genres': ['Animation', 'Adventure', 'Comedy'],
                'id': movie.id,
                'languages': ['English'],
                'metascore': 84,
                'plot': 'When a green ogre named Shrek discovers his swamp (...)',  # noqa
                'poster': 'https://m.media-amazon.com/images/M/MV5BOGZhM2FhNTItODAzNi00YjA0LWEyN2UtNjJlYWQzYzU1MDg5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',  # noqa
                'producer': 'Dreamworks',
                'ratings': [
                    {'source': 'Internet Movie Database', 'value': '7.9/10'},
                    {'source': 'Rotten Tomatoes', 'value': '88%'}
                ],
                'released': '2001-05-18',
                'runtime': 90,
                'title': 'Shrek',
                'website': 'http://www.shrek.com/',
                'writers': ['William Steig', 'Ted Elliott'],
                'year': 2001,
                'imdb_votes': 123,
                'imdb_rating': '2.3',
            }
        ]
    }


@pytest.mark.django_db
def test__list_movies__search_by_title(client):
    movie = factories.MovieFactory(
        title='Shrek',
    )
    factories.MovieFactory(
        title='Star Wars',
    )

    url = reverse('api:v1_movies:movies-list')

    response = client.get(
        url,
        data={
            'search': 'shr',
        },
    )

    assert response.status_code == 200
    data = response.data
    assert data['count'] == 1
    assert data['results'][0]['id'] == movie.id


@pytest.mark.django_db
def test__list_movies__ordering(client):
    movie_1 = factories.MovieFactory(
        year=2011
    )
    movie_2 = factories.MovieFactory(
        year=2012,
    )
    movie_3 = factories.MovieFactory(
        year=2000,
        imdb_votes=1,
        imdb_rating=1,
    )
    movie_5 = factories.MovieFactory(
        year=2000,
        imdb_votes=2,
        imdb_rating=2,
    )
    movie_4 = factories.MovieFactory(
        year=2000,
        imdb_votes=2,
        imdb_rating=1,
    )

    url = reverse('api:v1_movies:movies-list')

    response = client.get(
        url,
        data={
            'ordering': 'year,imdb_votes,imdb_rating',
        },
    )

    assert response.status_code == 200
    assert [
               movie['id'] for movie in response.data['results']
           ] == [
        movie_3.id, movie_4.id, movie_5.id, movie_1.id, movie_2.id
    ]


@pytest.mark.django_db
def test__list_movies__filter_by_year(client):
    movie = factories.MovieFactory(
        year=2011
    )
    factories.MovieFactory(
        year=2012,
    )
    factories.MovieFactory(
        year=2000,
    )

    url = reverse('api:v1_movies:movies-list')

    response = client.get(
        url,
        data={
            'year': 2011,
        },
    )

    assert response.status_code == 200
    data = response.data
    assert data['count'] == 1
    assert data['results'][0]['id'] == movie.id


@pytest.mark.django_db
def test__list_movies__filter_by_min_year(client):
    movie_1 = factories.MovieFactory(
        year=2011
    )
    movie_2 = factories.MovieFactory(
        year=2012,
    )
    factories.MovieFactory(
        year=2000,
    )

    url = reverse('api:v1_movies:movies-list')

    response = client.get(
        url,
        data={
            'min_year': 2011,
        },
    )

    assert response.status_code == 200
    data = response.data
    assert data['count'] == 2
    assert {
               movie['id'] for movie in data['results']
           } == {
        movie_1.id, movie_2.id
    }


@pytest.mark.django_db
def test__list_movies__filter_by_max_year(client):
    movie_1 = factories.MovieFactory(
        year=2011
    )
    movie_2 = factories.MovieFactory(
        year=2000,
    )
    factories.MovieFactory(
        year=2012,
    )

    url = reverse('api:v1_movies:movies-list')

    response = client.get(
        url,
        data={
            'max_year': 2011,
        },
    )

    assert response.status_code == 200
    data = response.data
    assert data['count'] == 2
    assert {
               movie['id'] for movie in data['results']
           } == {
        movie_1.id, movie_2.id
    }


@pytest.mark.django_db
def test__list_movies__filter_by_genres(client):
    movie_1 = factories.MovieFactory(
        genres=[
            factories.GenreFactory(
                name='Animation',
            ),
        ],
    )
    movie_2 = factories.MovieFactory(
        genres=[
            factories.GenreFactory(
                name='Adventure',
            ),
            factories.GenreFactory(
                name='Fantasy',
            ),
        ],
    )
    factories.MovieFactory(
        genres=[
            factories.GenreFactory(
                name='Comedy',
            ),
        ],
    )

    url = reverse('api:v1_movies:movies-list')

    response = client.get(
        url,
        data={
            'genres': ['Animation', 'Fantasy'],
        },
    )

    assert response.status_code == 200
    data = response.data
    assert data['count'] == 2
    assert {
               movie['id'] for movie in data['results']
           } == {
        movie_1.id, movie_2.id
    }


@pytest.mark.django_db
def test__list_movies__filter_by_non_existing_genre(client):
    url = reverse('api:v1_movies:movies-list')

    response = client.get(
        url,
        data={
            'genres': ['Animation', 'Fantasy'],
        },
    )

    assert response.status_code == 200

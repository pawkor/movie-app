from datetime import date
from decimal import Decimal
from unittest.mock import patch

import pytest
from django.urls import reverse

from movies import models, exceptions
from movies.tests import factories


@pytest.mark.parametrize(
    'data,expected_status_code',
    (
            ({}, 400),
            ({'title': 'Shrek'}, 201),
    )
)
@pytest.mark.django_db
@patch('movies.viewsets.imdb_client.get_movie_data')
def test__create_movie__requires_title(
        get_movie_data, data, expected_status_code, client
):
    get_movie_data.return_value = {}

    url = reverse('api:v1_movies:movies-list')

    response = client.post(
        url,
        data=data,
    )

    assert response.status_code == expected_status_code


@pytest.mark.django_db
@patch('movies.viewsets.imdb_client.get_movie_data')
def test__create_movie(
        get_movie_data, client
):
    factories.GenreFactory(
        name='Animation',
    )

    get_movie_data.return_value = {
        'title': 'Another title',
        'imdb_votes': 123,
        'imdb_rating': Decimal('3.2'),
        'year': 2000,
        'released': date(year=2000, month=10, day=10),
        'runtime': 90,
        'plot': 'Test plot',
        'awards': 'Test awards',
        'poster': 'https://test.poster',
        'metascore': 84,
        'dvd': date(year=2001, month=10, day=10),
        'box_office': 1234,
        'website': 'https://test.website',
        'genres': ['Animation', 'Adventure', 'Comedy'],
        'directors': ['Andrew Adamson', 'Vicky Jenson'],
        'writers': ['Andrew Adamson', 'William Steig'],
        'actors': ['Andrew Adamson'],
        'languages': ['English', 'Polish'],
        'countries': ['USA', 'Poland'],
        'producer': 'Dreamworks',
        'ratings': [
            {
                'source': 'Internet Movie Database',
                'value': '7.9/10',
            },
            {
                'source': 'Rotten Tomatoes',
                'value': '88%',
            },
            {
                'source': 'Metacritic',
                'value': '84/100',
            }
        ],
    }

    url = reverse('api:v1_movies:movies-list')

    response = client.post(
        url,
        data={
            'title': 'Shrek',
        },
    )

    assert response.status_code == 201

    producers = models.Producer.objects.all()
    assert len(producers) == 1
    assert producers[0].name == 'Dreamworks'

    countries = models.Country.objects.all()
    assert len(countries) == 2
    assert {
               country.name for country in countries
           } == {
               'USA', 'Poland'
           }

    languages = models.Language.objects.all()
    assert len(languages) == 2
    assert {
               language.name for language in languages
           } == {
               'English', 'Polish'
           }

    people = models.Person.objects.all()
    assert len(people) == 3
    assert {
               person.name for person in people
           } == {
               'Andrew Adamson', 'Vicky Jenson', 'William Steig'
           }

    genres = models.Genre.objects.all()
    assert len(genres) == 3
    assert {
               genre.name for genre in genres
           } == {
               'Animation', 'Adventure', 'Comedy'
           }

    movies = models.Movie.objects.all()
    assert len(movies) == 1
    movie = movies[0]

    assert movie.title == 'Another title'
    assert movie.imdb_votes == 123
    assert movie.imdb_rating == Decimal('3.2')
    assert movie.year == 2000
    assert movie.released == date(year=2000, month=10, day=10)
    assert movie.runtime == 90
    assert movie.plot == 'Test plot'
    assert movie.awards == 'Test awards'
    assert movie.poster == 'https://test.poster'
    assert movie.metascore == 84
    assert movie.dvd == date(year=2001, month=10, day=10)
    assert movie.box_office == 1234
    assert movie.website == 'https://test.website'
    assert {
               genre.name for genre in movie.genres.all()
           } == {
               'Animation', 'Adventure', 'Comedy'
           }
    assert {
               person.name for person in movie.directors.all()
           } == {
               'Andrew Adamson', 'Vicky Jenson'
           }
    assert {
               person.name for person in movie.writers.all()
           } == {
               'Andrew Adamson', 'William Steig'
           }
    assert {
               person.name for person in movie.actors.all()
           } == {
               'Andrew Adamson'
           }
    assert {
               language.name for language in movie.languages.all()
           } == {
               'English', 'Polish'
           }
    assert {
               country.name for country in movie.countries.all()
           } == {
               'USA', 'Poland'
           }
    assert movie.producer.name == 'Dreamworks'
    assert movie.ratings.count() == 3


@pytest.mark.parametrize(
    'exception',
    (
            exceptions.MovieNotFoundException,
            exceptions.ImdbCallFailedException,
            exceptions.ImdbDataParsingException,
    )
)
@patch('movies.viewsets.imdb_client.get_movie_data')
def test__create_movie__400_when_movie_not_found(
        get_movie_data, exception, client
):
    get_movie_data.side_effect = exception

    url = reverse('api:v1_movies:movies-list')

    response = client.post(
        url,
        data={
            'title': 'Shrek',
        },
    )

    assert response.status_code == 400

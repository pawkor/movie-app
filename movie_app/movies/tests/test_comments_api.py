import pytest
from django.urls import reverse

from movies import models
from movies.tests import factories


@pytest.mark.django_db
def test__add_comment(client):
    movie = factories.MovieFactory()
    comment_body = 'Great movie'

    url = reverse('api:v1_movies:comments-list')

    response = client.post(
        url,
        data={
            'movie': movie.id,
            'body': comment_body,
        },
    )

    comments = models.Comment.objects.all()
    assert len(comments) == 1
    comment = comments[0]
    assert comment.movie_id == movie.id
    assert comment.body == comment_body

    assert response.status_code == 201
    assert response.json() == {
        'id': comment.id,
        'movie': movie.id,
        'body': comment_body,
    }


@pytest.mark.django_db
def test__add_comment_not_existing_movie(client):
    url = reverse('api:v1_movies:comments-list')

    response = client.post(
        url,
        data={
            'movie': 1,
            'body': 'Great movie',
        },
    )

    assert response.status_code == 400


@pytest.mark.django_db
def test__list_comments(client):
    comment = factories.CommentFactory(
        body='Great movie',
    )

    url = reverse('api:v1_movies:comments-list')

    response = client.get(url)

    assert response.status_code == 200
    assert response.json() == {
        'count': 1,
        'next': None,
        'previous': None,
        'results': [
            {
                'id': comment.id,
                'movie': comment.movie_id,
                'body': 'Great movie',
            },
        ],
    }


@pytest.mark.django_db
def test__list_comments__filter_by_movie(client):
    movie_1 = factories.MovieFactory()
    comment_1 = factories.CommentFactory(
        movie=movie_1,
    )
    comment_2 = factories.CommentFactory(
        movie=movie_1,
    )
    factories.CommentFactory()

    url = reverse('api:v1_movies:comments-list')

    response = client.get(
        url,
        data={
            'movie': movie_1.id,
        },
    )

    assert response.status_code == 200
    data = response.data
    assert data['count'] == 2
    assert {
               movie['id'] for movie in data['results']
           } == {
        comment_1.id, comment_2.id
    }

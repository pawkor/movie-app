import factory

from movies import models


class ProducerFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Producer

    name = factory.Faker('company')


class RatingSourceFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.RatingSource

    name = factory.Faker('company')


class PersonFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Person

    name = factory.Faker('name')


class CountryFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Country

    name = factory.Faker('country')


class LanguageFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Language

    name = 'English'


class GenreFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Genre

    name = 'Animation'


class MovieFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Movie

    title = factory.Faker('sentence')
    year = factory.Faker('year')
    released = factory.Faker('past_date')
    runtime = factory.Faker('pyint')
    plot = factory.Faker('sentence')
    awards = factory.Faker('sentence')
    poster = factory.Faker('uri')
    metascore = factory.Faker('pyint')
    dvd = factory.Faker('past_date')
    box_office = factory.Faker('pyint')
    website = factory.Faker('uri')
    producer = factory.SubFactory(ProducerFactory)
    imdb_votes = factory.Faker('pyint')
    imdb_rating = factory.Faker('pydecimal', left_digits=1, right_digits=1, positive=True)  # noqa

    @factory.post_generation
    def genres(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for genre in extracted:
                self.genres.add(genre)

    @factory.post_generation
    def directors(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for director in extracted:
                self.directors.add(director)

    @factory.post_generation
    def writers(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for writer in extracted:
                self.writers.add(writer)

    @factory.post_generation
    def actors(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for actor in extracted:
                self.actors.add(actor)

    @factory.post_generation
    def languages(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for language in extracted:
                self.languages.add(language)

    @factory.post_generation
    def countries(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for country in extracted:
                self.countries.add(country)


class RatingFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Rating

    movie = factory.SubFactory(MovieFactory)
    source = factory.SubFactory(RatingSourceFactory)
    value = '10/100'


class CommentFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Comment

    movie = factory.SubFactory(MovieFactory)
    body = factory.Faker('sentence')

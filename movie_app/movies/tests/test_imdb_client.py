from datetime import date
from decimal import Decimal

import pytest

from movies import exceptions, imdb_client


def test__get_movie_data__raises_exception_when_movie_not_found(
        requests_mock, settings
):
    settings.IMDB_APIKEY = 'apikey'
    requests_mock.get(
        'http://www.omdbapi.com/?apikey=apikey&t=Shrek&type=movie&plot=full',
        body=open('movies/tests/imdb_responses/not_found.json', 'rb'),
    )
    with pytest.raises(exceptions.MovieNotFoundException):
        imdb_client.get_movie_data('Shrek')


def test__get_movie_data__raises_exception_when_imdb_call_failed(
        requests_mock, settings
):
    settings.IMDB_APIKEY = 'apikey'
    requests_mock.get(
        'http://www.omdbapi.com/?apikey=apikey&t=Shrek&type=movie&plot=full',
        status_code=503,
    )
    with pytest.raises(exceptions.ImdbCallFailedException):
        imdb_client.get_movie_data('Shrek')


def test__get_movie_data__parse_data(
        requests_mock, settings
):
    settings.IMDB_APIKEY = 'apikey'
    requests_mock.get(
        'http://www.omdbapi.com/?apikey=apikey&t=Shrek&type=movie&plot=full',
        body=open('movies/tests/imdb_responses/shrek.json', 'rb'),
    )

    assert imdb_client.get_movie_data('Shrek') == {
        'title': 'Shrek',
        'imdb_votes': 549161,
        'imdb_rating': Decimal('7.9'),
        'year': 2001,
        'released': date(year=2001, month=5, day=18),
        'runtime': 90,
        'plot': "When a green ogre named Shrek discovers his swamp has been 'swamped' with all sorts of fairytale creatures by the scheming Lord Farquaad, Shrek sets out with a very loud donkey by his side to 'persuade' Farquaad to give Shrek his swamp back. Instead, a deal is made. Farquaad, who wants to become the King, sends Shrek to rescue Princess Fiona, who is awaiting her true love in a tower guarded by a fire-breathing dragon. But once they head back with Fiona, it starts to become apparent that not only does Shrek, an ugly ogre, begin to fall in love with the lovely princess, but Fiona is also hiding a huge secret.",  # noqa
        'awards': 'Won 1 Oscar. Another 36 wins & 60 nominations.',
        'poster': 'https://m.media-amazon.com/images/M/MV5BOGZhM2FhNTItODAzNi00YjA0LWEyN2UtNjJlYWQzYzU1MDg5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',  # noqa
        'metascore': 84,
        'dvd': date(year=2001, month=11, day=2),
        'box_office': 266982666,
        'website': 'http://www.shrek.com/',
        'genres': ['Animation', 'Adventure', 'Comedy', 'Family', 'Fantasy'],
        'directors': ['Andrew Adamson', 'Vicky Jenson'],
        'writers': ['William Steig', 'Ted Elliott', 'Terry Rossio', 'Joe Stillman', 'Roger S.H. Schulman', 'Cody Cameron', 'Chris Miller', 'Conrad Vernon'],  # noqa
        'actors': ['Mike Myers', 'Eddie Murphy', 'Cameron Diaz', 'John Lithgow'],  # noqa
        'languages': ['English'],
        'countries': ['USA'],
        'producer': 'Dreamworks',
        'ratings': [
            {
                'source': 'Internet Movie Database',
                'value': '7.9/10',
            },
            {
                'source': 'Rotten Tomatoes',
                'value': '88%',
            },
            {
                'source': 'Metacritic',
                'value': '84/100',
            }
        ],
    }


def test__get_movie_data__with_na(
        requests_mock, settings
):
    settings.IMDB_APIKEY = 'apikey'
    requests_mock.get(
        'http://www.omdbapi.com/?apikey=apikey&t=Shrek&type=movie&plot=full',
        body=open('movies/tests/imdb_responses/with_na.json', 'rb'),
    )

    data = imdb_client.get_movie_data('Shrek')

    assert 'awards' not in data
    assert 'metascore' not in data
    assert 'dvd' not in data
    assert 'producer' not in data


def test__get_movie_data__failing_data(
        requests_mock, settings
):
    settings.IMDB_APIKEY = 'apikey'
    requests_mock.get(
        'http://www.omdbapi.com/?apikey=apikey&t=Shrek&type=movie&plot=full',
        body=open('movies/tests/imdb_responses/failing.json', 'rb'),
    )

    with pytest.raises(exceptions.ImdbDataParsingException):
        imdb_client.get_movie_data('Shrek')

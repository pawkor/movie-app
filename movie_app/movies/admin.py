from django.contrib import admin

from movies import models

admin.site.register(
    [
        models.Country,
        models.Language,
        models.Person,
        models.Genre,
        models.Rating,
        models.RatingSource,
        models.Producer,
        models.Movie,
        models.Comment,
    ]
)

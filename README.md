# run local app
1. `sudo docker-compose up web`
2. go to http://localhost:80/api/v1/docs/ to see documentation

# run tests
`sudo docker-compose up test`

# create admin user
1. `sudo docker-compose run web python manage.py shell`
2. `from django.contrib.auth.models import User; User.objects.create_superuser(username='admin', email='admin@movie.com', password='admin')` 


# movie list features

## filter by:
* year
* min_year
* max_year
* genres (multisearch)

## order by:
* year
* imdb_votes
* imdb_rating

## search by:
* title

example call: localhost:80/api/v1/movies/?genres=Animation&genres=Adventure&ordering=year,imbd_votes&search=sh

## assumptions:
* when a movie is not found in omdb, there is any HTTP error or error while parsing data I do not save the item in database

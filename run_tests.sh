#!/bin/sh

set -o nounset
set -o errexit

until ping -w 1 db >> /dev/null; do
  sleep 1
done

python -m pytest --cov-config=../coveragerc.ini --disable-pytest-warnings --cov=. $@ && flake8
